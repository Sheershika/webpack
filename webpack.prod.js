const common = require("./webpack.config");
const {merge} = require("webpack-merge")
const path = require("path")

module.exports = merge(common, {
    mode: "production",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "bundle.[contentHash].js"
    }, 
});

